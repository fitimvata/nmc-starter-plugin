<?php

namespace Plugin\Base;

abstract class Singleton
{
    private static $instances = [];
    protected function __construct() {}
    public static function init()
    {
        if (!isset(self::$instances[static::class])) {
            self::$instances[static::class] = new static();
        }
        return self::$instances[static::class];
    }

    public static function getInstance() {
        return static::init();
    }

    private function __clone() {}
    private function __wakeup() {}
} 