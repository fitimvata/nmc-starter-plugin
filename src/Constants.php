<?php

namespace Plugin;




final class Constants
{
    const PREFIX = 'nmc_starter_plugin';
    const BASE_DIR = NMC_STARTER_PLUGIN_BASE_DIR;
    const NAME = NMC_STARTER_PLUGIN_NAME;
    const JS_GLOBAL_VARS = NMC_STARTER_PLUGIN_NAME;
}