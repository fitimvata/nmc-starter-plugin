<?php

namespace Plugin;

use Plugin\Base\Singleton;
use Plugin\Constants;

class Plugin extends Singleton
{

    /**
     * @var string
     */
    public $version;
    /**
     * @var bool
     */
    public $scripts_in_footer = true;
    /**
     * @var string
     */
    public $resource_dir = 'assets';

    /**
     * @var array asset mix manifest
     */
    public $manifest = [];

    /**
     * @var array
     */
    public $stylesheet_files = [
        '/styles/app.css',
    ];

    /**
     * @var array
     */
    public $scripts_files = [
        '/scripts/app.js' => [
            'key' => 'app',
            'in_footer' => true,
            'deps' => ['jquery']
        ],
    ];

    /**
     * @var array
     */
    public $scripts_urls = [
    ];

    protected function __construct()
    {
        $this->set_version();
        $this->set_mix_manifest();
        add_action('wp_head', [$this, 'js_vars']);
        add_action('wp_enqueue_scripts', [$this, 'load_assets']);

    }

    /**
     * Read version from composer.json
     * @return void
     */
    private function set_version()
    {
        $content = file_get_contents(Constants::BASE_DIR . DIRECTORY_SEPARATOR . 'composer.json');
        $content = json_decode($content, true);
        $this->version = $content['version'] ?? '1.0.0';
    }

    /**
     * Read mix-manifest for compiled assets
     * @return void
     */
    private function set_mix_manifest()
    {
        $content = file_get_contents(Constants::BASE_DIR . DIRECTORY_SEPARATOR . $this->resource_dir . DIRECTORY_SEPARATOR . 'mix-manifest.json');
        $content = json_decode($content, true);
        $this->manifest = $content;
    }

    /**
     * register some js variables
     *
     * @see https://codex.wordpress.org/Function_Reference/wp_localize_script
     */
    public function js_vars()
    {

        $INITIAL_DATA = apply_filters(\prefix() . '_js_initial_data', [
            'ajax_url' => admin_url('admin-ajax.php'),
        ]);

        ?>
            <script type="text/javascript">
			    window["<?php echo Constants::JS_GLOBAL_VARS; ?>"] = <?php echo wp_json_encode($INITIAL_DATA); ?>;
		    </script>
        <?php

    }

    public function load_assets()
    {
        $this->load_stylesheet_files();
        $this->load_scripts_files();
    }

    protected function load_stylesheet_files()
    {
        $uri = plugins_url( '/' ) . Constants::NAME . '/' . $this->resource_dir;
        $path = Constants::BASE_DIR . DIRECTORY_SEPARATOR . $this->resource_dir;

        foreach ($this->stylesheet_files as $key => $file) {
            $mix_file = $this->manifest[$file] ?? $file;
            if (file_exists($path . $file)) {
                wp_enqueue_style(
                    Constants::NAME . '-style-' . $key, // handle
                    $uri . $mix_file, // src
                    [], // deps
                    $this->version, // version
                    'all' // media
                );
            }
        }
    }

    protected function load_scripts_files()
    {

        foreach ($this->scripts_urls as $key => $url) {
            wp_enqueue_script(
                Constants::NAME . '-scripts-url-' . $key, // handle
                $url, // src
                [], // deps
                null, // version
                $this->scripts_in_footer // in_footer
            );
        }

        $uri = plugins_url( '/' ) . Constants::NAME . '/' . $this->resource_dir;
        $path = Constants::BASE_DIR . DIRECTORY_SEPARATOR . $this->resource_dir;

        foreach ($this->scripts_files as $file => $options) {
            $mix_file = $this->manifest[$file] ?? $file;

            if (file_exists($path . $file)) {
                wp_enqueue_script(
                    Constants::NAME . '-scripts-' . $options['key'], // handle
                    $uri . $mix_file, // src
                    $options['deps'] ?? [],
                    $this->version, // version
                    $options['in_footer'] // in_footer
                );
            }
        }
    }
}