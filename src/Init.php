<?php

namespace Plugin;

use Plugin\Base\Singleton;

class Init extends Singleton
{

    protected function __construct()
    {
        Plugin::init();
    }
}