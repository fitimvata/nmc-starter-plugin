<?php
/**
 * Plugin Name:     Nmc Starter Plugin
 * Plugin URI:      PLUGIN SITE HERE
 * Description:     PLUGIN DESCRIPTION HERE
 * Author:          YOUR NAME HERE
 * Author URI:      YOUR SITE HERE
 * Text Domain:     nmc-starter-plugin
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Nmc_Starter_Plugin
 */

// Your code starts here.
define('NMC_STARTER_PLUGIN_BASE_DIR', __DIR__);
define('NMC_STARTER_PLUGIN_NAME', plugin_basename(__DIR__) );


require __DIR__ . '/build/vendor/autoload.php';
\NmcStarterPlugin\Plugin\Init::init();
