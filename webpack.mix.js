const mix = require('laravel-mix');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'assets/scripts')
    .sass('resources/assets/sass/app.scss', 'assets/styles')
    .setPublicPath('assets')
    // HACK to fix problems with url in css files
    .setResourceRoot('../../assets/')
    .disableNotifications()

if (mix.inProduction()) {
    mix.version();
}